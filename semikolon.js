var express = require('express')
var user=require("../semikolon/Controler/UserControler");
var authControler=require("../semikolon/Controler/authControler")
var db=require("./db/db");
var app = express()
app.use(express.json());
app.use("/api/user",user);
app.use("/api/auth",authControler);

app.get('/', function (req, res) {
  res.send('Hello World')
})
app.get('/getMyName', function (req, res) {
  res.send('Sobhan  Mozsafaris')
})
 
app.listen(3000)
