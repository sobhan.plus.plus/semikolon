const express = require('express');
const bcrypt = require('bcrypt');
const auth=require("../middleware/auth")
 const _user=require("../Entity/userEntity")
 const CircularJSON = require('circular-json');
var router = express.Router();

var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
 

router.get('/',auth(["user"]) , async function (req, res) {
  res.send('Sobhan Mozafari')
})

router.get('/getuser',auth(["user"]),async function (req, res) {
var user=await _user.findById({ _id:req.user  })
if(!user) return res.status(400).send("User not Found....");
else
return res.send(user);
})

router.post('/register', async function (req, res) {
  const salt= await bcrypt.genSalt(10);
    let hashPassword=await  bcrypt.hash(req.body.password,salt);
  await   User.create({
    name : req.body.name,
    email : req.body.email,
    password : hashPassword,
    phone : req.body.phone,
    role:["user"]
}), 
  res.send(User)
})

router.post('/favoriteword',auth(["user"]) , async function (req, res) {
  var user=await _user.findById({ _id:req.user  })
  var a={en:req.body.fa,fa:req.body.en};
  console.log(req.body);
  user.words.push(a);
  await user.save();
return  res.status(200).send(":)");
})

  module.exports=router;