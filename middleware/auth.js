const jwt = require('jsonwebtoken');
 

module.exports = 
function HasRole(role) {
return  function (req, res, next) {
  const token = req.header('x-auth-token');
  if (!token) return res.status(401).send('Access denied. No token provided.');
 
  try {
 
    const decoded = jwt.verify(token, 'shhhhh');
    let found = role.some(r=> decoded.role.includes(r))
    req.user = decoded._id; 
    next();
  }
  catch (ex) {
    res.status(400).send('Invalid token.');
  }
}
} 